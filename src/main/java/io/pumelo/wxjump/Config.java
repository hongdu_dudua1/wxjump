package io.pumelo.wxjump;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Runtime getRuntime(){
        return Runtime.getRuntime();
    }
}
