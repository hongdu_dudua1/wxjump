package io.pumelo.wxjump;

import org.bytedeco.javacpp.opencv_core;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;

@SpringBootApplication
public class WxjumpApplication implements CommandLineRunner{

	@Autowired
	private AutoJumpService autoJumpService;

	public static void main(String[] args) {
		SpringApplication.run(WxjumpApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		while (true){
			opencv_core.IplImage targetImg = autoJumpService.getScreenShot();
			Point chessLocation = autoJumpService.findChess(targetImg);
			if(autoJumpService.isGameOver()){
				break;
			}
			Point nextPoint = autoJumpService.findNextTop(targetImg,chessLocation);
//			nextPoint.y+=20;
			double distance = nextPoint.distance(chessLocation);
			autoJumpService.jump(distance);
			Thread.sleep(1000);
		}
	}
}
